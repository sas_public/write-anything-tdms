# Read/Write Anything TDMS

This is an implementation of the MGI Read/Write Anything, or the OpenG Config vis that allows the user to read or write multiple values from/to tdms metadata.

The end result will be a .vipm package

I am using this project as an experiment in TDD - Test Drive Developent.  I presented this at GDevCon2.  It should be available on the GDevCon YouTube Channel.

My plan is to write Unit Tests before starting any of the coding.

